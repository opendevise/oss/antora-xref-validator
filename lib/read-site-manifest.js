'use strict'

const expandPath = require('@antora/expand-path-helper')
const { promises: fsp } = require('fs')
const get = require('got')
const getCacheDir = require('cache-directory')
const ospath = require('path')
const { ungzip: gunzip } = require('node-gzip')
const { URL } = require('url')

const URI_SCHEME_RX = /^https?:\/\//

function downloadSiteManifest (playbook, url) {
  const { cacheDir, fetch } = playbook.runtime || {}
  const resolvedCacheDir = resolveCacheDir(cacheDir, playbook.dir || '.')
  const cacheFile = ospath.join(resolvedCacheDir, `${new URL(url).hostname}-manifest.json`)
  return (fetch
    ? Promise.resolve(false)
    : fsp
      .access(cacheFile)
      .then(() => true)
      .catch(() => false)
  ).then((exists) =>
    exists
      ? cacheFile
      : fsp
        .mkdir(resolvedCacheDir, { recursive: true })
        .then(() => get(url, { resolveBodyOnly: true, responseType: 'buffer' }))
        .then((body) => fsp.writeFile(cacheFile, body).then(() => cacheFile))
  )
}

function resolveCacheDir (cacheDir, startDir) {
  return cacheDir == null
    ? getCacheDir('antora') || ospath.resolve('.antora/cache')
    : expandPath(cacheDir, '~+', startDir)
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}

async function readSiteManifest (playbook, manifestUrl) {
  const manifestPath = await (isUrl(manifestUrl)
    ? downloadSiteManifest(playbook, manifestUrl)
    : expandPath(manifestUrl, '~+', playbook.dir))
  try {
    return JSON.parse(
      await (manifestPath.endsWith('.gz')
        ? fsp.readFile(manifestPath).then(gunzip)
        : fsp.readFile(manifestPath, 'utf-8'))
    )
  } catch {
    return {}
  }
}

module.exports = readSiteManifest
