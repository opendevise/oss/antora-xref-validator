= Antora Xref Validator

A custom Antora site generator that performs rudimentary validation of xrefs (specifically page references).
Scans links in the converted documents (pages and nav files) to discover unresolved xrefs.
If any unresolved xrefs are detected, it prints a report and exits with a failure code.

Despite being a generator, this extension does not output a site.
Instead, it outputs a report of unresolved xrefs by component version to stderr, if any are found, then terminate with a non-zero exit status.
If no unresolved xrefs are found, it will exit normally without emitting any messages.

This generator is a short-term solution to provide xref validation for Antora.
It will eventually be replaced or rewritten to perform validation that's more thorough and better integrated into the site generation process.

== Installation

 $ npm install -g gitlab:antora/xref-validator

or, from this project's directory:

 $ npm link

== Usage

 $ NODE_PATH="$(npm -g root)" antora --generator @antora/xref-validator antora-playbook.yml

NOTE: The `NODE_PATH` assignment is necessary to ensure Antora can locate node modules install globally.
Depending on your environment, you may find that this assignment is unnecessary.
If you've installed Antora globally using Yarn, you may need to add `"$(yarn global dir)/node_modules"` to the `NODE_PATH` environment variable instead.

== Copyright and License

Copyright (C) 2018-2020 OpenDevise Inc.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.
